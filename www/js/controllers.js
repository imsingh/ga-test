angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope, $cordovaDevice,$ionicPlatform, $cordovaGoogleAnalytics) {
  $ionicPlatform.ready(function() {

    $scope.$on("$ionicView.enter", function() {
      $cordovaGoogleAnalytics.trackView("Dash View").then(function() {
        console.log("Let's see if it works");
      });

      //window.ga.trackView("Hello 2");
    });
  });
})

.controller('ChatsCtrl', function($scope, Chats, $ionicPlatform, $cordovaGoogleAnalytics) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $ionicPlatform.ready(function() {
    $scope.$on("$ionicView.enter", function() {
      $cordovaGoogleAnalytics.trackView("Chats View").then(function() {
        console.log("Let's see if it works");
      });

      //window.ga.trackView("Hello 2");
    });
  });

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});
